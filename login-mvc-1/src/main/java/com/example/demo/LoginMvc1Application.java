package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LoginMvc1Application {

	public static void main(String[] args) {
		SpringApplication.run(LoginMvc1Application.class, args);
	}

}
